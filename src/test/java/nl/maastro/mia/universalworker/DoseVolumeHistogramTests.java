package nl.maastro.mia.universalworker;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DoseVolumeHistogramTests {

    private static final BigDecimal[] vVolume = {new BigDecimal(1),new BigDecimal(3),new BigDecimal(10)};
    private static final BigDecimal volume = new BigDecimal(30);
    private static final double RELATIVE_ERROR = 0.005d;

    private DoseVolumeHistogram doseVolumeHistogram;

    @Before
    public void setup(){
        doseVolumeHistogram = new DoseVolumeHistogram();
        doseVolumeHistogram.setvVolume(vVolume);
        doseVolumeHistogram.setVolume(volume);
    }

    @Test
    public void testGetvVolumeRelative(){
        BigDecimal[] volumeRelative = doseVolumeHistogram.getvVolumeRelative();
        assertEquals(3.33d,volumeRelative[0].doubleValue(), RELATIVE_ERROR);
        assertEquals(10.00d,volumeRelative[1].doubleValue(), RELATIVE_ERROR);
        assertEquals(33.33d,volumeRelative[2].doubleValue(), RELATIVE_ERROR);
    }
}