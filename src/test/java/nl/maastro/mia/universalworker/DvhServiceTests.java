package nl.maastro.mia.universalworker;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.maastro.mia.universalworker.service.computations.dvh.DvhService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DvhServiceTests {
	private static final Logger logger = Logger.getLogger(DvhServiceTests.class);
	
	private static final BigDecimal V48 = new BigDecimal(70.611);
	private static final BigDecimal V48_ABSOLUTE = new BigDecimal(40.9212);
	private static final BigDecimal D2 = new BigDecimal(49.9198);
	private static final BigDecimal D2_PERCENTAGE = new BigDecimal(110.933);
	private static final BigDecimal VOLUME = new BigDecimal(57.9528);
	private static final BigDecimal TARGET_PRESCRIPTION_DOSE = new BigDecimal(45);
	private static final BigDecimal RELATIVE_ERROR = new BigDecimal(0.005);
	
	private static BigDecimal[] vVolume = new BigDecimal[]{};
	private static BigDecimal[] vDose = new BigDecimal[]{};

	@Autowired
	private DvhService dvhService;
	
	@Before
	public void setup(){
		URL url = Thread.currentThread().getContextClassLoader().getResource("vVolume.txt");
		vVolume = readVector(url.getPath());
		url = Thread.currentThread().getContextClassLoader().getResource("vDose.txt");
		vDose = readVector(url.getPath());
	}

	@Test
	public void testComputeRelVolumeWithAbsDose(){
		BigDecimal v48 = dvhService.computeRelVolumeWithAbsDose(vVolume, vDose, new BigDecimal(48));
		assertEquals(V48.doubleValue(),v48.doubleValue(), V48.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeRelVolumeWithRelDose(){
		BigDecimal limit = new BigDecimal(48).divide(TARGET_PRESCRIPTION_DOSE,5,RoundingMode.HALF_UP).multiply(new BigDecimal(100));
		BigDecimal v48 = dvhService.computeRelVolumeWithRelDose(vVolume, vDose, limit, TARGET_PRESCRIPTION_DOSE);
		assertEquals(V48.doubleValue(),v48.doubleValue(), V48.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeAbsVolumeWithAbsDose(){
		BigDecimal v48 = dvhService.computeAbsVolumeWithAbsDose(vVolume, vDose, new BigDecimal(48));
		assertEquals(V48_ABSOLUTE.doubleValue(),v48.doubleValue(), V48.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeAbsVolumeWithRelDose(){
		BigDecimal limit = new BigDecimal(48).divide(TARGET_PRESCRIPTION_DOSE,5,RoundingMode.HALF_UP).multiply(new BigDecimal(100));
		BigDecimal v48 = dvhService.computeAbsVolumeWithRelDose(vVolume, vDose, limit, TARGET_PRESCRIPTION_DOSE);
		assertEquals(V48_ABSOLUTE.doubleValue(),v48.doubleValue(), V48.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeAbsDoseWithRelVolume(){
		BigDecimal d2 = dvhService.computeAbsDoseWithRelVolume(vVolume, vDose, new BigDecimal(2));
		assertEquals(D2.doubleValue(),d2.doubleValue(), D2.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeRelDoseWithRelVolume(){
		BigDecimal d2 = dvhService.computeRelDoseWithRelVolume(vVolume,
				vDose,
				new BigDecimal(2),
				TARGET_PRESCRIPTION_DOSE);
		assertEquals(D2_PERCENTAGE.doubleValue(),d2.doubleValue(), D2_PERCENTAGE.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeRelDoseWithAbsVolume(){
		BigDecimal d2 = dvhService.computeRelDoseWithAbsVolume(vVolume,
															vDose,
															new BigDecimal(2),
															TARGET_PRESCRIPTION_DOSE);
		assertEquals(D2_PERCENTAGE.doubleValue(),d2.doubleValue(), D2_PERCENTAGE.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeAbsDoseWithAbsVolume(){
		BigDecimal d2 = dvhService.computeAbsDoseWithAbsVolume(vVolume, 
				vDose, 
				(new BigDecimal(2).divide(new BigDecimal(100))).multiply(VOLUME));
		assertEquals(D2.doubleValue(),d2.doubleValue(), D2.multiply(RELATIVE_ERROR).doubleValue());
	}
	
	@Test
	public void testComputeAbsVolumeWithAbsDoseReturnsZero(){
		BigDecimal v60 = dvhService.computeAbsVolumeWithAbsDose(vVolume, vDose, new BigDecimal(60));
		assertEquals(v60.doubleValue(), 0.0, 0.0);
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testComputeAbsDoseWithAbsVolumeThrowsIllegalArgumentExceptionForNegativeVolume(){
		dvhService.computeAbsDoseWithAbsVolume(vVolume, vDose, new BigDecimal(-10));
	}
	
	private BigDecimal[] readVector(String filename) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(filename));
			String line = br.readLine();
			String[] numbers = line.split(",");
			return StringToBigDecimal(numbers);
		} catch (IOException e) {
			logger.error("Problem reading vector textfile",e);
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException e) {
				logger.error("Problem reading vector textfile",e);
			}

		}
		return null;
	}
	
	private BigDecimal[] StringToBigDecimal(String[] numbers) {
		BigDecimal[] decimals = new BigDecimal[numbers.length];
		for(int i = 0; i < numbers.length; i++){
			decimals[i] = new BigDecimal(Double.parseDouble(numbers[i]));
		}
		return decimals;
	}
}