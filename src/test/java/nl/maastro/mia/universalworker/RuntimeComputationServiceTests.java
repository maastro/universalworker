package nl.maastro.mia.universalworker;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.mia.universalworker.service.computations.RuntimeComputationService;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.Image;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RuntimeComputationServiceTests {

    private static final String COMPUTATIONDATA_JSON_PATH = "computationData.json";
    private static final String CT_SERIES_DIR = "\\\\dev-build.maastro.nl\\testdata\\12345\\CT";
    private static final String CT_IMAGE_PATH = CT_SERIES_DIR + "\\FO-169300684774812893.dcm";
    private static final String RTSTRUCT_PATH = "\\\\dev-build.maastro.nl\\testdata\\12345\\RTSTRUCT\\FO-4073997332899944647.dcm";
    private static final String YAML_FILE_PATH = "\\\\dev-build.maastro.nl\\testdata\\pyradiomics\\params.yml";
    
    private ComputationData computationData = new ComputationData();
    private Map<String, Path> extConfigFiles = new HashMap<>();

    private ObjectMapper objectMapper;

    private RuntimeComputationService runtimeComputationService;
    
    @Before
    public void setup() throws Exception{
        this.objectMapper = new ObjectMapper();
        RuntimeComputationService runtimeComputationServiceInstance = new RuntimeComputationService(objectMapper, null, null, null, null);
        runtimeComputationService = Mockito.spy(runtimeComputationServiceInstance);
        Mockito.doReturn(COMPUTATIONDATA_JSON_PATH).when(runtimeComputationService).writeComputationDataJson(Matchers.any(ComputationData.class));
        
        Image ctImage = new Image();
        ctImage.setLocation(CT_IMAGE_PATH);
        
        Image rtStructImage = new Image();
        rtStructImage.setLocation(RTSTRUCT_PATH);
        
        computationData.setTempPath(".\\temp");
        computationData.getImages().put("CT", Arrays.asList(ctImage));
        computationData.getImages().put("RTSTRUCT", Arrays.asList(rtStructImage));
        
        VolumeOfInterest voi = new VolumeOfInterest();
        voi.setName("Lungs-PTV");
        voi.setRois(Arrays.asList("Lung_L", "Lung_R", "PTV-1"));
        voi.setOperators(Arrays.asList("+", "-"));
        computationData.setVolumesOfInterest(Arrays.asList(voi));
        
        extConfigFiles.put("paramsFile", Paths.get(YAML_FILE_PATH));
    }
    
    @Test
    public void resolveComputationDataPlaceholderTest() throws Exception {
        String computationDataPath = runtimeComputationService.resolvePlaceholder("computationdata", computationData, new HashMap<>());
        assertEquals(COMPUTATIONDATA_JSON_PATH, computationDataPath);
    }
    
    @Test
    public void resolveCtPlaceholderTest() throws Exception {
        String ctDir = runtimeComputationService.resolvePlaceholder("ct", computationData, new HashMap<>());
        assertEquals(CT_SERIES_DIR, ctDir);
    }
    
    @Test
    public void resolveRtStructPlaceholderTest() throws Exception {
        String rtStructPath = runtimeComputationService.resolvePlaceholder("rtstruct", computationData, new HashMap<>());
        assertEquals(RTSTRUCT_PATH, rtStructPath);
    }
    
    @Test
    public void resolveVolumesOfInterestPlaceholderTest() throws Exception {
        String volumesOfInterestJson = runtimeComputationService.resolvePlaceholder("voi", computationData, new HashMap<>());
        assertEquals(objectMapper.writeValueAsString(computationData.getVolumesOfInterest()), volumesOfInterestJson);
    }
    
    @Test
    public void resolveExtFilePlaceholderTest() throws Exception {
        Map<String, Path> extConfigFiles = new HashMap<>();
        extConfigFiles.put("paramsFile", Paths.get(YAML_FILE_PATH));
        String extConfigFilePath = runtimeComputationService.resolvePlaceholder("paramsFile", computationData, extConfigFiles);
        assertEquals(YAML_FILE_PATH, extConfigFilePath);
    }
    
    @Test
    public void unresolvablePlaceholderTest() {
        String unresolvablePlaceholder = "unresolvable";
        try {     
            runtimeComputationService.resolvePlaceholder(unresolvablePlaceholder, computationData, new HashMap<>());
            Assert.fail("Expected exception is not thrown");
        } catch (Exception e) {
            String expectedMessage = "Unable to resolve unknown placeholder: " + unresolvablePlaceholder; 
            assertEquals(expectedMessage, e.getMessage());
        }
    }
    
    @Test
    public void resolveCommandTestA() throws Exception {
        Map<String, Path> extConfigFiles = new HashMap<>();
        extConfigFiles.put("paramsFile", Paths.get(YAML_FILE_PATH));
        
        List<String> inputCommand = Arrays.asList(
                "cmd.exe", "/c", "script.bat", 
                "${computationdata}", 
                "${paramsFile}", 
                "12345");
        
        List<String> expectedOutputCommand = Arrays.asList(
                "cmd.exe", "/c", "script.bat", 
                COMPUTATIONDATA_JSON_PATH, 
                YAML_FILE_PATH, 
                "12345");
        
        List<String> outputCommand = runtimeComputationService.resolveCommand(inputCommand, computationData, extConfigFiles);
        assertEquals(expectedOutputCommand, outputCommand);
    }
    
    @Test
    public void resolveCommandTestB() throws Exception {
        Map<String, Path> extConfigFiles = new HashMap<>();
        extConfigFiles.put("paramsFile", Paths.get(YAML_FILE_PATH));
        
        List<String> inputCommand = Arrays.asList("cmd.exe", "/c", "script.bat", 
                "${ct}", 
                "${rtstruct}");
        
        List<String> expectedOutputCommand = Arrays.asList("cmd.exe", "/c", "script.bat", 
                CT_SERIES_DIR, 
                RTSTRUCT_PATH);
        
        List<String> outputCommand = runtimeComputationService.resolveCommand(inputCommand, computationData, extConfigFiles);
        assertEquals(expectedOutputCommand, outputCommand);
    }
    
    @Test
    public void unresolvableCommandTest() {
        List<String> inputCommand = Arrays.asList("cmd.exe", "/c", "script.bat", "${unresolvablePlaceholder}");
        try {
            runtimeComputationService.resolveCommand(inputCommand, computationData, new HashMap<>());
            Assert.fail("Expected exception is not thrown");
        } catch (Exception e) {
            String expectedMessage = "Unable to resolve placeholder 'unresolvablePlaceholder' in command: " + inputCommand;
            assertEquals(expectedMessage, e.getMessage());
        }
    }
    
    @Test
    public void runCmdCommandTest() throws Exception {
        ComputationResult result = new ComputationResult();
        List<String> command = Arrays.asList("cmd.exe", "/c", "ping", "sanderputs.com", "-n", "3");
        String workingDirectory = ".";
        result = runtimeComputationService.runCommand(command, workingDirectory, result);
        assertTrue(result.getError()==null);
    }
    
    @Test
    public void runPythonCommandTest() throws Exception {
        Assume.assumeTrue(isPythonInstalled());
        ComputationResult result = new ComputationResult();
        List<String> command = Arrays.asList("cmd.exe", "/c", "python", "-c", "print 'Hello World!'");
        String workingDirectory = ".";
        result = runtimeComputationService.runCommand(command, workingDirectory, result);
        assertTrue(result.getError()==null);
    }
    
    @Test
    public void runInvalidCommandTest() throws Exception {
        ComputationResult result = new ComputationResult();
        List<String> command = Arrays.asList("invalidcommand");
        String workingDirectory = ".";
        try {
            result = runtimeComputationService.runCommand(command, workingDirectory, result);
            Assert.fail("Expected exception is not thrown");
        } catch (Exception e) {
            assertTrue(e instanceof IOException);
            assertTrue(e.getMessage().startsWith("Cannot run program \"invalidcommand\""));
            assertTrue(e.getMessage().endsWith("The system cannot find the file specified"));
        }
    }
    
    private static boolean isPythonInstalled() {
        try {
            Runtime.getRuntime().exec("python --version");
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
