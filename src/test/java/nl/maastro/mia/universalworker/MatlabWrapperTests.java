package nl.maastro.mia.universalworker;

import calculateDose.DvhUtilities;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mathworks.toolbox.javabuilder.MWCellArray;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWMatrixRef;
import com.mathworks.toolbox.javabuilder.MWStructArray;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.CreateStructureDataService;
import nl.maastro.mia.universalworker.service.MatlabService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MatlabWrapperTests {
	private static final List<String> ROI_LIST = Collections.singletonList("GTV-1");
	private static final String RTSTRUCT_LOCATION = "\\\\dev-build.maastro.nl\\testdata\\12345\\RTSTRUCT\\FO-4073997332899944647.dcm";
	private static final String RTDOSE_LOCATION = "\\\\dev-build.maastro.nl\\testdata\\12345\\RTDOSE\\FO-3153671375338877408_v2.dcm";

	private static final String DOSE_UNIT = "Gy";
	private static final String VOLUME_UNIT = "cc";
	private static final double REF_DMIN = 45.4715;
	private static final double REF_DMEAN = 48.3739;
	private static final double REF_DMAX = 50.7382;
	private static final double REF_VOLUME = 58.2733;
	private static final double RELATIVE_ERROR = 0.001;
	
	private DvhUtilities dvhUtilities;
	private MWStructArray ctProperties;
	
	@Value("${computations.binsize:0.01}")
	private double binSize;
	
	@Autowired
	CreateStructureDataService createStructureDataService;
	
	@Autowired
	MatlabService matlabService;

	@Before
	public void setup() throws MWException, IOException, JsonParseException, JsonMappingException {
		dvhUtilities = new DvhUtilities();
		
		String[] fieldNames = new String[]{"Rows","Columns","SliceThickness","PixelSpacing",
				"ImagePositionPatient","ImageOrientationPatient","CTFileLength"};
		ctProperties = new MWStructArray(1, 1, fieldNames);
		ctProperties.set("PixelSpacing", 1, new double[] {0.9765625,0.9765625});
		ctProperties.set("ImagePositionPatient", 1, new double[] {-249.5117,-415.5117,-685.5000});
		ctProperties.set("ImageOrientationPatient", 1, new double[] {1,0,0,0,1,0});
		ctProperties.set("SliceThickness", 1, new double[] {3});
		ctProperties.set("Rows", 1, 512);
		ctProperties.set("Columns", 1, 512);
		ctProperties.set("CTFileLength", 1, new Double(136));
	}
	
	@After
	public void teardown() {
		dvhUtilities.dispose();
	}
	
	@Test
	public void testCreateDvh() throws MWException, IOException, JsonParseException, JsonMappingException {

		BigDecimal targetPrescriptionDose = new BigDecimal(-1);

		MWMatrixRef referenceImage = matlabService.createReferenceImage(ctProperties, dvhUtilities);
		MWMatrixRef referenceDose = matlabService.createReferenceDose(RTDOSE_LOCATION, targetPrescriptionDose, referenceImage, dvhUtilities);
        MWMatrixRef rtStruct = matlabService.createRtStruct(RTSTRUCT_LOCATION, dvhUtilities);
		
		DoseVolumeHistogram dvh = matlabService.createDvh(rtStruct,
				referenceDose,
				referenceImage,
				MatlabService.convertStringListToMWCellArray(ROI_LIST),
				new MWCellArray(),
				dvhUtilities);

		assertEquals(REF_DMIN, dvh.getMinDose().doubleValue(), RELATIVE_ERROR * REF_DMIN);
		assertEquals(REF_DMEAN, dvh.getMeanDose().doubleValue(), RELATIVE_ERROR * REF_DMEAN);
		assertEquals(REF_DMAX, dvh.getMaxDose().doubleValue(), RELATIVE_ERROR * REF_DMAX);
		assertEquals(VOLUME_UNIT, dvh.getVolumeUnit());
		assertEquals(DOSE_UNIT, dvh.getDoseUnit());
		assertEquals(REF_VOLUME, dvh.getVolume().doubleValue(), RELATIVE_ERROR * REF_VOLUME);
	}

	@Test
	public void testCreateDvhNoDose() throws MWException, IOException, JsonParseException, JsonMappingException {
		MWMatrixRef referenceImage = matlabService.createReferenceImage(ctProperties, dvhUtilities);
		MWMatrixRef rtStruct = matlabService.createRtStruct(RTSTRUCT_LOCATION, dvhUtilities);

		DoseVolumeHistogram dvh = matlabService.createDvh(rtStruct,
				referenceImage,
				MatlabService.convertStringListToMWCellArray(ROI_LIST),
				new MWCellArray(),
				dvhUtilities);
		
		assertEquals(VOLUME_UNIT, dvh.getVolumeUnit());
		assertEquals(REF_VOLUME, dvh.getVolume().doubleValue(), RELATIVE_ERROR * REF_VOLUME);
	}
}