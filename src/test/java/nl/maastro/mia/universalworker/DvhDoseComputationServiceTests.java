package nl.maastro.mia.universalworker;

import java.util.Collections;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.maastro.mia.universalworker.service.computations.dvh.DvhDoseComputationService;
import nl.maastro.mia.workerplugin.container.Computation;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.DataContainer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DvhDoseComputationServiceTests {
	
	@Autowired
	private DvhDoseComputationService dvhDoseComputationService;
	
	@Test
	public void testIncompleteComputationData(){
		DataContainer dataContainer = new DataContainer();
		dataContainer.setContainerId(1l);
		dataContainer.setCreationTime(new Date());
		dataContainer.setImages(Collections.emptyMap());
		dataContainer.setPatientId("12345");
		dataContainer.setPlanLabel("1p1b1d1a");
		Computation computation = new Computation();
		ComputationData computationData = new ComputationData(dataContainer, computation);
		ComputationResult result = dvhDoseComputationService.performComputation(computationData);
		Assert.assertEquals("computation data null or incomplete.",result.getError());
	}
}