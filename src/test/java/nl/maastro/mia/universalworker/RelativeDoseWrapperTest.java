package nl.maastro.mia.universalworker;

import calculateDose.DvhUtilities;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWMatrixRef;
import com.mathworks.toolbox.javabuilder.MWStructArray;
import nl.maastro.mia.universalworker.config.ComputationProperties;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.universalworker.service.MatlabService;
import nl.maastro.mia.universalworker.service.computations.dvh.DoseComputationService;
import nl.maastro.mia.universalworker.service.computations.dvh.VolumeComputationService;
import nl.maastro.mia.universalworker.service.computations.dvh.enumeration.DoseMetric;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RelativeDoseWrapperTest {

    private static final String RTDOSE_LOCATION =   "\\\\dev-build.maastro.nl\\testdata\\clinical_validation_dataset\\relative\\P00208C0006I13350141\\RTDOSE_1.3.6.1.4.1.32722.208.6.333019913620517767932602821392770657334\\1.3.6.1.4.1.32722.208.6.333019913620517767932602821392770657334.dcm";
    private static final String RTSTRUCT_LOCATION = "\\\\dev-build.maastro.nl\\testdata\\clinical_validation_dataset\\relative\\P00208C0006I13350141\\RTSTRUCT_1.3.6.1.4.1.32722.208.6.242247963679794643836046385701520388749\\1.3.6.1.4.1.32722.208.6.242247963679794643836046385701520388749.dcm";

    private DvhUtilities dvhUtilities;
    private MWStructArray ctProperties;
    private static final double RELATIVE_ERROR = 0.05d;

    private String roiNoData = "Lungs-PTV_5";
    private String roiNotPresent = "IAMNOTPRESENT";

    private static final BigDecimal targetPrescriptionDose = new BigDecimal(60.5);
    private String rtogLungGtv = "Lungs-GTV";
    private String roiLungsGtv = "Lungs-GTV";
    private double roiLungsGtvVolume = 4412.89;
    private double roiLungGtvMin =  0.22;
    private double roiLungGtvMean = 14.86;
    private double roiLungGtvMax = 66.44;

    private String rtogPtv = "PTV";
    private String roiPTV1 = "PTV1";
    private double roiPtvVolume = 316.12;
    private double roiPtvMin =  0.22;
    private double roiPtvMean = 61.14;
    private double roiPtvMax = 67.67;

    private Map<String, DoseVolumeHistogram> dvhs;
    private MWMatrixRef referenceDose;
    private MWMatrixRef referenceImage;

    private MatlabService matlabService;
    private DoseComputationService doseComputationService;
    private VolumeComputationService volumeComputationService;



    @Before
    public void setup() throws MWException, IOException, JsonParseException, JsonMappingException {

        ObjectMapper objectMapper = new ObjectMapper();
        GenericComputationToolService genericComputationToolService = new GenericComputationToolService();

        this.matlabService = new MatlabService(new ComputationProperties(), objectMapper);
        this.doseComputationService = new DoseComputationService(genericComputationToolService, objectMapper);
        this.volumeComputationService = new VolumeComputationService(genericComputationToolService);

        dvhUtilities = new DvhUtilities();

        String[] fieldNames = new String[]{"Rows","Columns","SliceThickness","PixelSpacing",
                "ImagePositionPatient","ImageOrientationPatient","CTFileLength"};
        ctProperties = new MWStructArray(1, 1, fieldNames);
        ctProperties.set("PixelSpacing", 1, new double[] {0.9765625,0.9765625});
        ctProperties.set("ImagePositionPatient", 1, new double[] {-249.51171875,-430.51171875,-625});
        ctProperties.set("ImageOrientationPatient", 1, new double[] {1,0,0,0,1,0});
        ctProperties.set("SliceThickness", 1, new double[] {3});
        ctProperties.set("Rows", 1, 512);
        ctProperties.set("Columns", 1, 512);
        ctProperties.set("CTFileLength", 1, new Double(132));
        referenceImage = matlabService.createReferenceImage(ctProperties, dvhUtilities);
        referenceDose = matlabService.createReferenceDose(RTDOSE_LOCATION, targetPrescriptionDose, referenceImage, dvhUtilities);
    }

    @After
    public void teardown() {
        dvhUtilities.dispose();
    }

    @Test
    public void createRelativeRtDoseRoiPresent() throws Exception {
        dvhs = matlabService.createDvhs(getVolumeOfInterestPresent(),
                dvhUtilities,
                getRtStruct(),
                referenceDose,
                referenceImage);

        DoseVolumeHistogram dvhLungsGtv = dvhs.get(rtogLungGtv);
        BigDecimal meanLungGtv = doseComputationService.getDoseMetric(dvhLungsGtv, DoseMetric.MEAN);
        assertEquals(roiLungGtvMean, meanLungGtv.doubleValue(), RELATIVE_ERROR*roiLungGtvMean);
        BigDecimal minLungGtv = doseComputationService.getDoseMetric(dvhLungsGtv, DoseMetric.MIN);
        assertEquals(roiLungGtvMin, minLungGtv.doubleValue(), RELATIVE_ERROR*roiLungGtvMin);
        BigDecimal maxLungGtv = doseComputationService.getDoseMetric(dvhLungsGtv, DoseMetric.MAX);
        assertEquals(roiLungGtvMax, maxLungGtv.doubleValue(), RELATIVE_ERROR*roiLungGtvMax);
        BigDecimal volumeLungGtv = volumeComputationService.getVolume(dvhLungsGtv);
        assertEquals(roiLungsGtvVolume, volumeLungGtv.doubleValue(), RELATIVE_ERROR*roiLungsGtvVolume);

        DoseVolumeHistogram dvhPtv= dvhs.get(rtogPtv);
        BigDecimal meanPtv = doseComputationService.getDoseMetric(dvhPtv, DoseMetric.MEAN);
        assertEquals(roiPtvMean, meanPtv.doubleValue(), RELATIVE_ERROR*roiPtvMean);
    }

    @Test(expected = MWException.class)
    public void createRelativeRtDoseRoiNotPresent() throws MWException, IOException {
        dvhs = matlabService.createDvhs(getVolumeOfInterestNotPresent(),
                dvhUtilities,
                getRtStruct(),
                referenceDose,
                referenceImage);
    }

    @Test(expected = MWException.class)
    public void createRelativeRtDoseRoiNoData() throws MWException, IOException {
        dvhs = matlabService.createDvhs(getVolumeOfInterestNoData(),
                dvhUtilities,
                getRtStruct(),
                referenceDose,
                referenceImage);
    }


    private Map<String, VolumeOfInterest> getVolumeOfInterestPresent(){
        VolumeOfInterest volumeOfInterest = new VolumeOfInterest();
        volumeOfInterest.setName(rtogLungGtv);
        volumeOfInterest.setRois(Arrays.asList(roiLungsGtv));

        VolumeOfInterest volumeOfInterestPtv = new VolumeOfInterest();
        volumeOfInterestPtv.setName(rtogPtv);
        volumeOfInterestPtv.setRois(Arrays.asList(roiPTV1));

        return new HashMap<String, VolumeOfInterest>(){{
            put(rtogLungGtv, volumeOfInterest);
            put(rtogPtv, volumeOfInterestPtv);
        }};
    }

    private Map<String, VolumeOfInterest> getVolumeOfInterestNotPresent(){
        VolumeOfInterest volumeOfInterest = new VolumeOfInterest();
        volumeOfInterest.setName(roiNotPresent);
        volumeOfInterest.setRois(Arrays.asList(roiNotPresent));
        return new HashMap<String, VolumeOfInterest>(){{
            put(roiNotPresent, volumeOfInterest);
        }};
    }

    private Map<String, VolumeOfInterest> getVolumeOfInterestNoData(){
        VolumeOfInterest volumeOfInterest = new VolumeOfInterest();
        volumeOfInterest.setName(roiNoData);
        volumeOfInterest.setRois(Arrays.asList(roiNoData));
        return new HashMap<String, VolumeOfInterest>(){{
            put(roiNoData, volumeOfInterest);
        }};
    }



    private MWMatrixRef getRtStruct() throws MWException {
        return matlabService.createRtStruct(RTSTRUCT_LOCATION, dvhUtilities);
    }

}
