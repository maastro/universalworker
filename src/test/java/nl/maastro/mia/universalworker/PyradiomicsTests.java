package nl.maastro.mia.universalworker;

import nl.maastro.mia.universalworker.service.computations.RuntimeComputationService;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.Image;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class PyradiomicsTests {
    
    private static final String COMPUTATIONDATA_PATH = "\\\\dev-build.maastro.nl\\testdata\\pyradiomics\\computationData.json";
    private static final String PARAMS_FILE_PATH = "\\\\dev-build.maastro.nl\\testdata\\pyradiomics\\params.yml";
    private static final String CT_IMAGE_PATH = "\\\\dev-build.maastro.nl\\testdata\\12345\\CT\\FO-169300684774812893.dcm";
    private static final String RTSTRUCT_PATH = "\\\\dev-build.maastro.nl\\testdata\\12345\\RTSTRUCT\\FO-4073997332899944647.dcm";
    private static final String WORKING_DIRECTORY = ".\\..\\pyradiomics-extension";
    
    private static final List<String> INPUT_COMMAND = Arrays.asList("cmd.exe", "/c", "python", "MiaWrapper.py", 
            "${computationData}", 
            "${paramsFile}", 
            ".\\PyradiomicsOutput");
    
    private static final List<String> RESOLVED_COMMAND = Arrays.asList("cmd.exe", "/c", "python", "MiaWrapper.py", 
            COMPUTATIONDATA_PATH, 
            PARAMS_FILE_PATH, 
            ".\\PyradiomicsOutput");
    
    private ComputationData computationData = new ComputationData();
    private Map<String, Path> extConfigFiles = new HashMap<>();
    
    private RuntimeComputationService runtimeComputationService;
    
    @Before
    public void setup() throws Exception{

        RuntimeComputationService runtimeComputationServiceInstance = new RuntimeComputationService(null, null, null, null, null);
        runtimeComputationService = Mockito.spy(runtimeComputationServiceInstance);
        Mockito.doReturn(COMPUTATIONDATA_PATH).when(runtimeComputationService).writeComputationDataJson(Matchers.any(ComputationData.class));
        
        Image ctImage = new Image();
        ctImage.setLocation(CT_IMAGE_PATH);
        
        Image rtStructImage = new Image();
        rtStructImage.setLocation(RTSTRUCT_PATH);
        
        computationData.setTempPath(".\\temp");
        computationData.getImages().put("CT", Arrays.asList(ctImage));
        computationData.getImages().put("RTSTRUCT", Arrays.asList(rtStructImage));
        
        extConfigFiles.put("paramsFile", Paths.get(PARAMS_FILE_PATH));
    }
    
    @Test
    public void resolveCommandTest() throws Exception {
        List<String> resolvedCommand = runtimeComputationService.resolveCommand(INPUT_COMMAND, computationData, extConfigFiles);
        assertEquals(RESOLVED_COMMAND, resolvedCommand);
    }
    
    @Test
    @Ignore
    public void runCommandTest() throws Exception {
        ComputationResult result = new ComputationResult();
        result = runtimeComputationService.runCommand(RESOLVED_COMMAND, WORKING_DIRECTORY, result);
        assertEquals(null, result.getError());
    }
}
