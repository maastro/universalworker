package nl.maastro.mia.universalworker.web.dto;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

import nl.maastro.mia.universalworker.constants.DicomTag;

public class CtPropertiesDto {
    
    @JsonProperty(DicomTag.SLICE_THICKNESS)
    private double sliceThickness;
    
    @JsonProperty(DicomTag.IMAGE_POSITION_PATIENT)
    private double[] imagePositionPatient = new double[3];
    
    @JsonProperty(DicomTag.IMAGE_ORIENTATION_PATIENT)
    private int[] imageOrientationPatient = new int[6];
    
    @JsonProperty(DicomTag.ROWS)
    private int rows;
    
    @JsonProperty(DicomTag.COLUMNS)
    private int columns;
    
    @JsonProperty(DicomTag.PIXEL_SPACING)
    private double[] pixelSpacing = new double[2];

    public double getSliceThickness() {
        return sliceThickness;
    }

    public void setSliceThickness(double sliceThickness) {
        this.sliceThickness = sliceThickness;
    }

    public double[] getImagePositionPatient() {
        return imagePositionPatient;
    }

    public void setImagePositionPatient(double[] imagePositionPatient) {
        this.imagePositionPatient = imagePositionPatient;
    }

    public int[] getImageOrientationPatient() {
        return imageOrientationPatient;
    }

    public void setImageOrientationPatient(int[] imageOrientationPatient) {
        this.imageOrientationPatient = imageOrientationPatient;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public double[] getPixelSpacing() {
        return pixelSpacing;
    }

    public void setPixelSpacing(double[] pixelSpacing) {
        this.pixelSpacing = pixelSpacing;
    }

    @Override
    public String toString() {
        return "CtPropertiesDto [sliceThickness=" + sliceThickness + ", imagePositionPatient="
                + Arrays.toString(imagePositionPatient) + ", imageOrientationPatient="
                + Arrays.toString(imageOrientationPatient) + ", rows=" + rows + ", columns=" + columns
                + ", pixelSpacing=" + Arrays.toString(pixelSpacing) + "]";
    }
	
}
