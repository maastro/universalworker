package nl.maastro.mia.universalworker.web.dto;

public class DvhCurveDto {

	private double[] volumeVector;
	private double[] doseVector;
	private double binsize;
	
	
	public double[] getVolumeVector() {
		return volumeVector;
	}
	public void setVolumeVector(double[] volumeVector) {
		this.volumeVector = volumeVector;
	}
	public double[] getDoseVector() {
		return doseVector;
	}
	public void setDoseVector(double[] doseVector) {
		this.doseVector = doseVector;
	}
	public double getBinsize() {
		return binsize;
	}
	public void setBinsize(double binsize) {
		this.binsize = binsize;
	}
}
