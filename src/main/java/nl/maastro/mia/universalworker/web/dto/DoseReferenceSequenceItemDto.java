package nl.maastro.mia.universalworker.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import nl.maastro.mia.universalworker.constants.DicomTag;

public class DoseReferenceSequenceItemDto {
    
    @JsonProperty(DicomTag.DOSE_REFERENCE_TYPE)
    private DoseReferenceType doseReferenceType;
    
    @JsonProperty(DicomTag.TARGET_PRESCRIPTION_DOSE)
    private Double targetPrescriptionDose;
    
    public DoseReferenceType getDoseReferenceType() {
        return doseReferenceType;
    }

    public void setDoseReferenceType(DoseReferenceType doseReferenceType) {
        this.doseReferenceType = doseReferenceType;
    }

    public Double getTargetPrescriptionDose() {
        return targetPrescriptionDose;
    }

    public void setTargetPrescriptionDose(Double targetPrescriptionDose) {
        this.targetPrescriptionDose = targetPrescriptionDose;
    }
    
    public static enum DoseReferenceType {
        ORGAN_AT_RISK, TARGET;
    }

}
