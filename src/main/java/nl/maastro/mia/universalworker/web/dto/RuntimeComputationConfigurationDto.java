package nl.maastro.mia.universalworker.web.dto;

import java.util.ArrayList;
import java.util.List;

import nl.maastro.mia.workerplugin.web.dto.ExtConfigFileDto;

public class RuntimeComputationConfigurationDto {
    
    private String runtimeComputationName;
    private List<ExtConfigFileDto> configFiles = new ArrayList<>();
    
    public String getRuntimeComputationName() {
        return runtimeComputationName;
    }
    public void setRuntimeComputationName(String runtimeComputationName) {
        this.runtimeComputationName = runtimeComputationName;
    }
    public List<ExtConfigFileDto> getConfigFiles() {
        return configFiles;
    }
    public void setConfigFiles(List<ExtConfigFileDto> configFiles) {
        this.configFiles = configFiles;
    }
}
