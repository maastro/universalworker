package nl.maastro.mia.universalworker.entities;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoseVolumeHistogram {
    private String volumeUnit;
	private String doseUnit;
	private BigDecimal volume;
	private BigDecimal minDose;
	private BigDecimal meanDose;
	private BigDecimal maxDose;
	private BigDecimal[] vVolume;
	private BigDecimal[] vDose;
	private double binSize;
    
	public String getVolumeUnit() {
		return volumeUnit;
	}
	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}
	public String getDoseUnit() {
		return doseUnit;
	}
	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public BigDecimal getMinDose() {
		return minDose;
	}
	public void setMinDose(BigDecimal minDose) {
		this.minDose = minDose;
	}
	public BigDecimal getMeanDose() {
		return meanDose;
	}
	public void setMeanDose(BigDecimal meanDose) {
		this.meanDose = meanDose;
	}
	public BigDecimal getMaxDose() {
		return maxDose;
	}
	public void setMaxDose(BigDecimal maxDose) {
		this.maxDose = maxDose;
	}
	public BigDecimal[] getvVolume() {
		return vVolume;
	}
	public void setvVolume(BigDecimal[] vVolume) {
		this.vVolume = vVolume;
	}
	public BigDecimal[] getvDose() {
		return vDose;
	}
	public void setvDose(BigDecimal[] vDose) {
		this.vDose = vDose;
	}
	public double getBinSize() {
		return binSize;
	}
	public void setBinSize(double binSize) {
		this.binSize = binSize;
	}
	public BigDecimal[] getvVolumeRelative() {
		BigDecimal[] relativeVolumes = new BigDecimal[vVolume.length];
		for(int i = 0; i < relativeVolumes.length; i++){
            relativeVolumes[i] = vVolume[i].divide(volume, 5, RoundingMode.HALF_UP)
                                           .multiply(new BigDecimal(100));
        }
		return relativeVolumes;
	}
}