package nl.maastro.mia.universalworker.service;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mathworks.toolbox.javabuilder.MWClassID;
import com.mathworks.toolbox.javabuilder.MWMatrixRef;
import com.mathworks.toolbox.javabuilder.MWNumericArray;
import com.mathworks.toolbox.javabuilder.MWStructArray;

import calculateDose.DvhUtilities;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.computations.dvh.DoseComputationService;
import nl.maastro.mia.universalworker.service.computations.dvh.DvhCurveComputationService;
import nl.maastro.mia.universalworker.service.computations.dvh.DvhDoseComputationService;
import nl.maastro.mia.universalworker.service.computations.dvh.DvhVolumeComputationService;
import nl.maastro.mia.universalworker.service.computations.dvh.VolumeComputationService;
import nl.maastro.mia.universalworker.web.dto.CtPropertiesDto;
import nl.maastro.mia.workerplugin.constants.DicomConcepts;
import nl.maastro.mia.workerplugin.container.DataContainer;
import nl.maastro.mia.workerplugin.container.Image;
import nl.maastro.mia.workerplugin.container.ModuleConfiguration;
import nl.maastro.mia.workerplugin.service.InitializingService;

@Service("createStructureData")
public class CreateStructureDataService extends InitializingService {
	private static final Logger logger = Logger.getLogger(CreateStructureDataService.class);
	
	public static final String TARGET_PRESCRIPTION_DOSE = "targetprescriptiondose";

	private Set<String> dvhModules = new HashSet<>(Arrays.asList(new String[]{
			DoseComputationService.SERVICE_NAME,
			VolumeComputationService.SERVICE_NAME,
			DvhDoseComputationService.SERVICE_NAME,
			DvhVolumeComputationService.SERVICE_NAME,
			DvhCurveComputationService.SERVICE_NAME
		}));

	private Set<String> requiredModalities = new HashSet<>(Arrays.asList(new String[]{
			DicomConcepts.CT,
			DicomConcepts.RTSTRUCT,
			DicomConcepts.RTPLAN,
			DicomConcepts.RTDOSE,
	}));

	private Set<String> downloadModalities = new HashSet<>(Arrays.asList(new String[]{
			DicomConcepts.RTSTRUCT,
			DicomConcepts.RTPLAN,
			DicomConcepts.RTDOSE,
	}));
	
	@Autowired
	private MatlabService matlabService;
	
    @Autowired
    private FileService fileService;

    @Autowired
	private GenericComputationToolService genericComputationToolService;
	public static final String DVH_DATA = "DVH_DATA";

	/**
	 * As an initializing step, the volume of interest is
	 * interpolated in MATLAB and all relevant dose information is added.
	 * This data is saved in the datacontainer so that later computation
	 * modules do not have to repeatedly load data.
	 * @throws Exception thrown when a matlab reading has failed.
	 */
	@Override
	public void initialize(DataContainer dataContainer) throws Exception {

		if(!isDvhConfigured(dataContainer)) {
			logger.info("No dvh calculations configured");
			return;
		}

		logger.info("Initializing data container");
		long startTime = System.currentTimeMillis();

		genericComputationToolService.checkRequiredModalitiesAreAvailable(requiredModalities, dataContainer.getImages());
		downloadModalities(dataContainer.getImages(), Paths.get(dataContainer.getTempPath()));

		DvhUtilities dvhUtilities = new DvhUtilities();
		dataContainer.setMatlabInstance(dvhUtilities);
		
		try{
			BigDecimal targetPrescriptionDose = getTargetPrescriptionDose(dataContainer);
			dataContainer.getData().put(TARGET_PRESCRIPTION_DOSE, targetPrescriptionDose);
		} catch (PrescriptionDoseException e) {
			logger.warn("Unable to query target prescription dose for plan in container with id: "
						+ dataContainer.getContainerId());
		}

		createDoseVolumeHistograms(dataContainer);
		
		long totalTime = System.currentTimeMillis() -  startTime;
		logger.debug("Finished initializing data container in: " + totalTime + " ms");
	}
	
	private DataContainer createDoseVolumeHistograms(DataContainer dataContainer) throws Exception {
		Map<String,List<Image>> images = dataContainer.getImages();

		String doseLocation = getDoseLocation(images.get(DicomConcepts.RTDOSE));
		String ctSopUid = images.get(DicomConcepts.CT).get(0).getSopUid();
		int ctSize = images.get(DicomConcepts.CT).size();
		String structLocation = images.get(DicomConcepts.RTSTRUCT).get(0).getLocation();
		BigDecimal targetPrescriptionDose = (BigDecimal) dataContainer.getData().get(TARGET_PRESCRIPTION_DOSE);

		DvhUtilities dvhUtilities = (DvhUtilities) dataContainer.getMatlabInstance();
		MWStructArray ctProperties = getCtProperties(ctSopUid, ctSize);
		MWMatrixRef referenceImage = matlabService.createReferenceImage(ctProperties, dvhUtilities);
		MWMatrixRef rtStruct = matlabService.createRtStruct(structLocation, dvhUtilities);

		logger.info("Computing dose-volume histograms");
		Map<String, DoseVolumeHistogram> dvhs;
		if (doseLocation !=null &&  !doseLocation.isEmpty()) {
			MWMatrixRef referenceDose = matlabService.createReferenceDose(doseLocation, targetPrescriptionDose, referenceImage, dvhUtilities);
			dvhs = matlabService.createDvhs(
					dataContainer.getVolumesOfInterest(),
					dvhUtilities,
					rtStruct, 
					referenceDose,
					referenceImage);
			
		} else {
			logger.warn("Missing RTDOSE, cannot create dose related computations for container " 
					+ dataContainer.getContainerId());
			dvhs = matlabService.createDvhs(
					dataContainer.getVolumesOfInterest(),
					dvhUtilities,
					rtStruct,
					referenceImage);
		}
		
		dataContainer.getData().put(DVH_DATA, dvhs);
		dataContainer.setMatlabInstance(dvhUtilities);
		return dataContainer;
	}

	private boolean isDvhConfigured(DataContainer dataContainer){
		Set<String> configuredModules = dataContainer.getModuleConfigurations().stream()
				.map(ModuleConfiguration::getModuleName).collect(Collectors.toSet());
		configuredModules.retainAll(dvhModules);
		return !configuredModules.isEmpty();
	}

	private String getDoseLocation(List<Image> images){
		if(images != null && !images.isEmpty()){
			return images.get(0).getLocation();
		}
		return null;
	}

	private void downloadModalities(Map<String, List<Image>> images, Path path) throws Exception{
		for(String modality : downloadModalities)
			fileService.downloadModality(modality, images, path);
	}

	public MWStructArray getCtProperties(String sopUid, int ctFileLenght) throws Exception {
		
		CtPropertiesDto ctPropertiesDto = fileService.getCtProperties(sopUid);
		
		String[] fieldnames = {"Rows","Columns","SliceThickness","PixelSpacing",
				"ImagePositionPatient","ImageOrientationPatient","CTFileLength"};
		MWStructArray ctPropertiesStruct = new MWStructArray(1, 1, fieldnames);
		
		MWNumericArray pixelSpacing = MWNumericArray.newInstance(new int[]{2}, ctPropertiesDto.getPixelSpacing(), MWClassID.DOUBLE);
		MWNumericArray imagePositionPatient =  MWNumericArray.newInstance(new int[]{3}, ctPropertiesDto.getImagePositionPatient(), MWClassID.DOUBLE);
		MWNumericArray imageOrientationPatient = MWNumericArray.newInstance(new int[]{6}, ctPropertiesDto.getImageOrientationPatient(), MWClassID.UINT16);

		ctPropertiesStruct.set("PixelSpacing", 1, pixelSpacing);
		ctPropertiesStruct.set("ImagePositionPatient", 1, imagePositionPatient);
		ctPropertiesStruct.set("ImageOrientationPatient", 1, imageOrientationPatient);
		ctPropertiesStruct.set("SliceThickness", 1, ctPropertiesDto.getSliceThickness());
		ctPropertiesStruct.set("Rows", 1, ctPropertiesDto.getRows());
		ctPropertiesStruct.set("Columns", 1, ctPropertiesDto.getColumns());
		ctPropertiesStruct.set("CTFileLength", 1, ctFileLenght);

		return ctPropertiesStruct;
	}
	
    public BigDecimal getTargetPrescriptionDose(DataContainer dataContainer) throws PrescriptionDoseException {
        List<Image> plans = dataContainer.getImages().get(DicomConcepts.RTPLAN);
        if(plans.size() > 1){
            logger.warn("More than one plan found in datacontainer, using the first plan found.");
        }
        if(plans == null || plans.isEmpty()){
            logger.warn("No RTPLAN found for datacontainer with id "
                        + dataContainer.getContainerId()
                        + ", skipping DVH Dose computation.");
            throw new PrescriptionDoseException();
        }

        Image rtPlan = plans.get(0);

        try{
        	Double targetPrescriptionDose = fileService.getTargetPrescriptionDose(rtPlan.getSopUid());
        	return new BigDecimal(targetPrescriptionDose);
        }catch(Exception e){
        	logger.warn("No target prescription dose found for plan in datacontainer with id " + dataContainer.getContainerId());
        	throw new PrescriptionDoseException();
        }
    }

	@Override
	public String getServiceName() {
		return null;
	}

	@Override
	public String getVersion() {
		return null;
	}

	class PrescriptionDoseException extends Exception {
		private static final long serialVersionUID = 6840620914664930587L;
	}
}