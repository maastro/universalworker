package nl.maastro.mia.universalworker.service.computations.dvh.enumeration;

public enum DoseMetric {

    MAX, MIN, MEAN

}
