package nl.maastro.mia.universalworker.service.computations.dvh;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.universalworker.service.computations.dvh.enumeration.DoseMetric;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.service.interfaces.RequirementData;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static nl.maastro.mia.universalworker.service.CreateStructureDataService.DVH_DATA;

@Service("DoseComputation")
public class DoseComputationService implements ComputationInterface{
	private static final Logger logger = Logger.getLogger(DoseComputationService.class);

	public static final String SERVICE_NAME = "DoseComputation";
	private static final String DOSE_OPERATION = "doseOperation";
	private static final String RESULT = "result";

	private ObjectMapper objectMapper;

	private GenericComputationToolService genericComputationToolService;

	public DoseComputationService(GenericComputationToolService genericComputationToolService, ObjectMapper objectMapper){
		this.genericComputationToolService = genericComputationToolService;
		this.objectMapper = objectMapper;
	}

	@Override
	public ComputationResult performComputation(ComputationData computationData) {
		ComputationResult result = new ComputationResult();
		result.setModuleName(SERVICE_NAME);
		result.setVersion(genericComputationToolService.getWorkerVersion());
		result.setExportResult(true);

		boolean computationDataIsValid = genericComputationToolService
				.computationDataIsValid(computationData);
		if(!computationDataIsValid){
			result.setError("computation data null or incomplete.");
			return result;
		}

		@SuppressWarnings("unchecked")
		HashMap<String, DoseVolumeHistogram> dvhMap =
				(HashMap<String, DoseVolumeHistogram>) computationData.getData().get(DVH_DATA);

		// At the moment all computations for the MIA have at most one volume of interest
		String voiName = computationData.getVolumesOfInterest().get(0).getName();

		try {
			DoseVolumeHistogram dvh = dvhMap.get(voiName);
			if (dvh == null || dvh.getvVolume() == null || dvh.getvDose() == null) {
				throw new Exception("No DVH created for volume of interest: " + voiName);
			}

			DoseConfiguration doseConfiguration = objectMapper.readValue(computationData.getConfiguration(), DoseConfiguration.class);
			String doseOperation = doseConfiguration.getOperation();

			BigDecimal dose = getDoseMetric(dvh, DoseMetric.valueOf(doseOperation.toUpperCase()));

			result.getValues().put(DOSE_OPERATION, doseOperation);
			result.getValues().put(RESULT, dose.setScale(4, RoundingMode.HALF_UP).toString());
			logger.info("Result of calculation " + computationData.getIdentifier() + " is " + dose);
			return result;

		} catch (Exception e) {
			logger.error("Unable to perform DoseComputation for voiName=" + voiName
					+ ", containerId=" + computationData.getContainerId(), e);
			result.setError(e.getMessage());
			return result;
		}
	}

	public BigDecimal getDoseMetric(DoseVolumeHistogram dvh, DoseMetric doseMetric) throws IllegalArgumentException {
		switch (doseMetric) {
			case MIN:
				return dvh.getMinDose();
			case MEAN:
				return dvh.getMeanDose();
			case MAX:
				return dvh.getMaxDose();
			default:
				throw new IllegalArgumentException("Unknown dose metric: " + doseMetric);
		}
	}

	private static class DoseConfiguration {
		private String operation;

		public String getOperation() {
			return operation;
		}
		public void setOperation(String operation) {
			this.operation = operation;
		}
	}

	@Override
	public String getServiceName() {
		return SERVICE_NAME;
	}

	@Override
	public RequirementData getRequirements() {
		RequirementData reqData = new RequirementData();
		reqData.setModalities(new HashSet<>(Arrays.asList(new String[]{"CT", "RTSTRUCT", "RTDOSE"})));
		return reqData;
	}
}
