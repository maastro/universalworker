package nl.maastro.mia.universalworker.service.computations.dvh;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

@Service
public class DvhService {
	
	/**
	 * Given a Dose Volume Histogram, returns the volume in which the dose exceeds the specified limit.
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The dose limit that should be exceeded (in Gy).
	 * @return The volume that received the specified limit or more (in cc).
	 */
	public BigDecimal computeAbsVolumeWithAbsDose(
			BigDecimal[] vVolume, 
			BigDecimal[] vDose, 
			BigDecimal limit){
		if(vVolume.length != vDose.length){
			throw new IllegalArgumentException();
		}
		for(int vectorIndex = 0; vectorIndex < vDose.length; vectorIndex++){
			BigDecimal doseValue = vDose[vectorIndex];
			if(doseValue.compareTo(limit) >= 0){
				return vVolume[vectorIndex];
			}
		}
		return BigDecimal.ZERO;
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the volume in which the dose exceeds the specified limit.
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The dose limit that should be exceeded (in percent of the target prescription dose).
	 * @param targetPrescriptionDose The target prescription dose (in Gy).
	 * @return The volume that received the specified limit or more (in cc).
	 */
	public BigDecimal computeAbsVolumeWithRelDose(
			BigDecimal[] vVolume, 
			BigDecimal[] vDose, 
			BigDecimal limit, 
			BigDecimal targetPrescriptionDose){
		BigDecimal absoluteLimit = relativeToAbsolute(limit, targetPrescriptionDose);
		return computeAbsVolumeWithAbsDose(vVolume, vDose, absoluteLimit);
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the volume in which the dose exceeds the specified limit.
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The dose limit that should be exceeded (in Gy).
	 * @return The volume that received the specified limit or more (in percent of the total volume).
	 */
	public BigDecimal computeRelVolumeWithAbsDose(
			BigDecimal[] vVolume, 
			BigDecimal[] vDose, 
			BigDecimal limit){
		BigDecimal absoluteVolume = computeAbsVolumeWithAbsDose(vVolume, vDose, limit);
		BigDecimal totalVolume = vVolume[0];
		return absoluteToRelative(absoluteVolume, totalVolume);
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the volume in which the dose exceeds the specified limit.
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The dose limit that should be exceeded (in percent of the target prescription dose).
	 * @param targetPrescriptionDose The target prescription dose (in Gy).
	 * @return The volume that received the specified limit or more (in percent of the total volume).
	 */
	public BigDecimal computeRelVolumeWithRelDose(
			BigDecimal[] vVolume, 
			BigDecimal[] vDose, 
			BigDecimal limit, 
			BigDecimal targetPrescriptionDose){
		BigDecimal absoluteLimit = relativeToAbsolute(limit, targetPrescriptionDose);
		return computeRelVolumeWithAbsDose(vVolume, vDose, absoluteLimit);
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the minimum dose delivered to a part of the volume. 
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The volume for which to calculate the minimum delivered dose (in cc).
	 * @return The minimum delivered dose (in Gy).
	 */
	public BigDecimal computeAbsDoseWithAbsVolume(
			BigDecimal[] vVolume, 
			BigDecimal[] vDose, 
			BigDecimal limit){
		if(vVolume.length != vDose.length){
			throw new IllegalArgumentException();
		}
		for(int vectorIndex = 0; vectorIndex < vVolume.length; vectorIndex++){
			BigDecimal volumeValue = vVolume[vectorIndex];
			if(volumeValue.compareTo(limit) <= 0){
				return vDose[vectorIndex];
			}
		}
		throw new IllegalArgumentException("Limit is lower than min volume. limit is either negative or the DVH curve is wrong.");
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the minimum dose delivered to a part of the volume. 
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The volume for which to calculate the minimum delivered dose (in percent of the total volume).
	 * @return The minimum delivered dose (in Gy).
	 */
	public BigDecimal computeAbsDoseWithRelVolume(
			BigDecimal[] vVolume, 
			BigDecimal[] vDose, 
			BigDecimal limit){
		BigDecimal totalVolume = vVolume[0];
		limit = relativeToAbsolute(limit, totalVolume);
		return computeAbsDoseWithAbsVolume(vVolume, vDose, limit);
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the minimum dose delivered to a part of the volume. 
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The volume for which to calculate the minimum delivered dose (in cc).
	 * @param targetPrescriptionDose The target prescription dose (in Gy).
	 * @return The minimum delivered dose (in percent of the target pescription dose).
	 */
	public BigDecimal computeRelDoseWithAbsVolume(
			BigDecimal[] vVolume,
			BigDecimal[] vDose,
			BigDecimal limit,
			BigDecimal targetPrescriptionDose){
		BigDecimal absoluteDose = computeAbsDoseWithAbsVolume(vVolume, vDose, limit);
		return absoluteToRelative(absoluteDose, targetPrescriptionDose);
	}
	
	/**
	 * Given a Dose Volume Histogram, returns the minimum dose delivered to a part of the volume. 
	 * @param vVolume The vector containing all volume values for a DVH (in cc).
	 * @param vDose The vector containing all dose values for a DVH (in Gy).
	 * @param limit The volume for which to calculate the minimum delivered dose (in percent of the total volume).
	 * @param targetPrescriptionDose The target prescription dose (in Gy).
	 * @return The minimum delivered dose (in percent of the target pescription dose).
	 */
	public BigDecimal computeRelDoseWithRelVolume(
			BigDecimal[] vVolume,
			BigDecimal[] vDose,
			BigDecimal limit,
			BigDecimal targetPrescriptionDose){
		BigDecimal totalVolume = vVolume[0];
		limit = relativeToAbsolute(limit, totalVolume);
		BigDecimal absoluteDose = computeAbsDoseWithAbsVolume(vVolume, vDose, limit);
		return absoluteToRelative(absoluteDose, targetPrescriptionDose);
	}
	
	private BigDecimal absoluteToRelative(BigDecimal value, BigDecimal total){
		return value.divide(total,5,RoundingMode.HALF_UP).multiply(new BigDecimal(100));
	}
	
	private BigDecimal relativeToAbsolute(BigDecimal value, BigDecimal total){
		return total.divide(new BigDecimal(100),5,RoundingMode.HALF_UP).multiply(value);
	}
}