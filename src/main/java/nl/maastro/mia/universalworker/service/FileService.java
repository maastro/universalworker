package nl.maastro.mia.universalworker.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.mia.universalworker.constants.DicomTag;
import nl.maastro.mia.universalworker.web.dto.CtPropertiesDto;
import nl.maastro.mia.universalworker.web.dto.DicomQueryDto;
import nl.maastro.mia.universalworker.web.dto.DoseReferenceSequenceItemDto;
import nl.maastro.mia.universalworker.web.dto.DoseReferenceSequenceItemDto.DoseReferenceType;

@Service("fileservice") @DependsOn("fileserviceplugin")
public class FileService extends nl.maastro.mia.workerplugin.service.communication.FileService {

	private static final Logger logger = Logger.getLogger(FileService.class);
	
	private static final String HTTP = "http://";
	private static final String API_DICOM_TAGS = "/api/dicomtags";

	@Value("${microservice.fileservice:fileservice}")
    private String fileServiceName;

	private ObjectMapper objectMapper;
    private RestTemplate restTemplate;
	
	public FileService(ObjectMapper objectMapper, RestTemplate restTemplate) {
        objectMapper.configure(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS, true);
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
    }

	public Double getTargetPrescriptionDose(String rtPlanSopInstanceUid) throws Exception {
	    logger.info("Querying target prescription dose for rtPlanSopInstanceUid: " + rtPlanSopInstanceUid);
	    
	    List<List<String>> dicomTags = new ArrayList<>();
	    dicomTags.add(Arrays.asList(
                DicomTag.DOSE_REFERENCE_SEQUENCE,
                DicomTag.DOSE_REFERENCE_TYPE));
        dicomTags.add(Arrays.asList(
                DicomTag.DOSE_REFERENCE_SEQUENCE,
                DicomTag.TARGET_PRESCRIPTION_DOSE));
        
        JsonNode attributeValues = queryDicomAttributes(rtPlanSopInstanceUid, dicomTags);
        JsonNode doseReferenceSequence = attributeValues.get(DicomTag.DOSE_REFERENCE_SEQUENCE);
        
        if (doseReferenceSequence != null) {
            for (int index=0; index < doseReferenceSequence.size(); index++) {
                JsonNode doseReferenceSequenceItem = doseReferenceSequence.get(index);
                DoseReferenceSequenceItemDto doseReferenceSequenceItemDto = objectMapper.treeToValue(
                        doseReferenceSequenceItem, DoseReferenceSequenceItemDto.class);
                if (DoseReferenceType.TARGET.equals(doseReferenceSequenceItemDto.getDoseReferenceType())) {
                    return doseReferenceSequenceItemDto.getTargetPrescriptionDose();
                }
            }
        }
        
        throw new Exception("No target prescription dose found for rtPlanSopInstanceUid=" + rtPlanSopInstanceUid);
	}
	
	public CtPropertiesDto getCtProperties(String ctSopInstanceUid) throws Exception {
	    List<List<String>> dicomTags = Arrays.asList(
	            Arrays.asList(DicomTag.SLICE_THICKNESS),
	            Arrays.asList(DicomTag.IMAGE_POSITION_PATIENT),
                Arrays.asList(DicomTag.IMAGE_ORIENTATION_PATIENT),
                Arrays.asList(DicomTag.ROWS),
                Arrays.asList(DicomTag.COLUMNS),
                Arrays.asList(DicomTag.PIXEL_SPACING));
	    JsonNode attributeValues = queryDicomAttributes(ctSopInstanceUid, dicomTags);
	    return objectMapper.treeToValue(
                attributeValues, CtPropertiesDto.class);
	}
	
	private JsonNode queryDicomAttributes(String sopInstanceUid, 
            List<List<String>> dicomTags) throws Exception {
        DicomQueryDto dicomQuery = new DicomQueryDto();
        dicomQuery.setSopInstanceUid(sopInstanceUid);
        dicomQuery.setDicomTags(dicomTags);
        String requestUrl = HTTP + fileServiceName + API_DICOM_TAGS;
        logger.info("Making REST request " + requestUrl + " with body: " + dicomQuery);
        HttpEntity<DicomQueryDto> requestBody = new HttpEntity<DicomQueryDto>(dicomQuery);
        return restTemplate.postForObject(requestUrl, requestBody, JsonNode.class);
    }
	
}