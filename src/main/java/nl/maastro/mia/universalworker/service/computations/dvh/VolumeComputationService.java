package nl.maastro.mia.universalworker.service.computations.dvh;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.workerplugin.constants.DicomConcepts;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.service.interfaces.RequirementData;

import static nl.maastro.mia.universalworker.service.CreateStructureDataService.DVH_DATA;

@Service("VolumeComputation")
public class VolumeComputationService implements ComputationInterface {
	private static final Logger logger = Logger.getLogger(VolumeComputationService.class);
	
	private static final String RESULT = "result";
	public static final String SERVICE_NAME = "VolumeComputation";

	private GenericComputationToolService genericComputationToolService;

	public VolumeComputationService(GenericComputationToolService genericComputationToolService){
		this.genericComputationToolService = genericComputationToolService;
	}

	@Override
	public ComputationResult performComputation(ComputationData computationData) {
	    ComputationResult result = new ComputationResult();
        result.setModuleName(SERVICE_NAME);
        result.setVersion(genericComputationToolService.getWorkerVersion());
        result.setExportResult(true);
        
		boolean computationDataIsValid = genericComputationToolService
				.computationDataIsValid(computationData);
		if(!computationDataIsValid){
			result.setError("computation data null or incomplete.");
			return result;
		}
		
		@SuppressWarnings("unchecked")
		HashMap<String, DoseVolumeHistogram> dvhMap = 
				(HashMap<String, DoseVolumeHistogram>) computationData.getData().get(DVH_DATA);

		// At the moment all computations for the MIA have at most one volume of interest
		String voiName = computationData.getVolumesOfInterest().get(0).getName();
		
		try {
			DoseVolumeHistogram dvh = dvhMap.get(voiName);
			if (dvh == null || dvh.getvVolume() == null || dvh.getvDose() == null) {
				throw new Exception("No DVH created for volume of interest: " + voiName);
			}
			BigDecimal volume = getVolume(dvh);
			result.getValues().put(RESULT, volume.setScale(4, RoundingMode.HALF_UP).toString());
			logger.info("Result of calculation " + computationData.getIdentifier() + " is " + volume);
			return result;
			
		} catch (Exception e) {
			logger.error("Unable to perform VolumeComputation for voiName=" + voiName 
					+ ", containerId=" + computationData.getContainerId(), e);
			result.setError(e.getMessage());
			return result;
		}
	}

	public BigDecimal getVolume(DoseVolumeHistogram dvh) throws Exception {
		return dvh.getVolume();
	}

	@Override
	public RequirementData getRequirements() {
		RequirementData reqData = new RequirementData();
		reqData.setModalities(new HashSet<>(
				Arrays.asList(
						new String[]{DicomConcepts.CT,
								DicomConcepts.RTSTRUCT})));
		return reqData;
	}
	
	@Override
	public String getServiceName() {
		return SERVICE_NAME;
	}
}