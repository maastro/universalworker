package nl.maastro.mia.universalworker.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import calculateDose.DvhUtilities;
import nl.maastro.mia.universalworker.UniversalWorkerApplication;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.Image;

@Service
public class GenericComputationToolService {
	private static final String DEVELOPMENT = "development";
	
	public String getWorkerVersion() {
	    if (UniversalWorkerApplication.version == null){
            return DEVELOPMENT;
        } else {
            return UniversalWorkerApplication.version;
        }
	}

	public boolean computationDataIsValid(ComputationData computationData) {
		if (computationData == null || 
			computationData.getImages() == null ||
			computationData.getMatlabInstance() == null ||
			computationData.getPatientId() == null ||
			computationData.getConfiguration() == null||
			!(computationData.getMatlabInstance() instanceof DvhUtilities)
				){
			return false;
		}
		return true;
	}


	public void checkRequiredModalitiesAreAvailable(Set<String> requiredModalities, Map<String, List<Image>> images) throws Exception {
		for(String requiredModality : requiredModalities) {
			if (!images.keySet().contains(requiredModality) || images.get(requiredModality).isEmpty())
				throw new Exception("Missing required modality:" + requiredModality);
		}
	}

}