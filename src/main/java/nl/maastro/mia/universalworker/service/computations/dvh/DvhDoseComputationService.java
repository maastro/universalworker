package nl.maastro.mia.universalworker.service.computations.dvh;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.CreateStructureDataService;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.service.interfaces.RequirementData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static nl.maastro.mia.universalworker.service.CreateStructureDataService.DVH_DATA;

@Service("DvhDoseComputation")
public class DvhDoseComputationService implements ComputationInterface {
	private static final Logger logger = Logger.getLogger(DvhDoseComputationService.class);

	public static final String SERVICE_NAME = "DvhDoseComputation";
	private static final String TARGET_PRESCRIPTION_DOSE = "targetPrescriptionDose";
	private static final String RESULT = "result";
	private static final String ABSOLUTE_OUTPUT = "absoluteOutput";
	private static final String LIMIT = "limit";
	private static final String LIMIT_UNIT = "limitUnit";
	private static final String CC = "cc";
	private static final String PERCENT = "%";
	
	private static final int ROUND_DIGITS = 4;

	@Autowired
	ObjectMapper jsonMapper;
	
	@Autowired
	private GenericComputationToolService genericComputationToolService;
	
	@Autowired
	private DvhService dvhService;

	@Override
	public ComputationResult performComputation(ComputationData computationData) {
		ComputationResult result = new ComputationResult();
        result.setModuleName(SERVICE_NAME);
        result.setVersion(genericComputationToolService.getWorkerVersion());
        result.setExportResult(true);
        
		boolean computationDataIsValid = genericComputationToolService
				.computationDataIsValid(computationData);
		if(!computationDataIsValid){
			result.setError("computation data null or incomplete.");
			return result;
		}
		
		@SuppressWarnings("unchecked")
		HashMap<String, DoseVolumeHistogram> dvhMap = (HashMap<String, DoseVolumeHistogram>) 
				computationData.getData().get(DVH_DATA);
		
		// At the moment all computations for the MIA have at most one volume of interest
		String voiName = computationData.getVolumesOfInterest().get(0).getName();
		
		try {
			DoseVolumeHistogram dvh = dvhMap.get(voiName);
			if (dvh == null || dvh.getvVolume() == null || dvh.getvDose() == null) {
				throw new Exception("No DVH created for volume of interest: " + voiName);
			}
			
			DvhDoseConfiguration dvhConfiguration = jsonMapper.readValue(computationData.getConfiguration(), DvhDoseConfiguration.class);
			
			BigDecimal targetPrescriptionDose = (BigDecimal) computationData.getData().get(CreateStructureDataService.TARGET_PRESCRIPTION_DOSE);
			if (targetPrescriptionDose == null){
				throw new Exception("No target prescription dose found for plan in container with id: " +
						computationData.getContainerId());
			}
			
			result.getValues().put(TARGET_PRESCRIPTION_DOSE, targetPrescriptionDose.setScale(ROUND_DIGITS, RoundingMode.HALF_UP).toString());
			result.getValues().put(LIMIT, dvhConfiguration.getLimit().setScale(ROUND_DIGITS, RoundingMode.HALF_UP).toString());
			result.getValues().put(LIMIT_UNIT, dvhConfiguration.getLimitUnit());
			result.getValues().put(ABSOLUTE_OUTPUT, Boolean.toString(dvhConfiguration.getAbsoluteOutput()));
			
			BigDecimal dose = calculateDvhD(dvh, dvhConfiguration, targetPrescriptionDose);
			result.getValues().put(RESULT, dose.setScale(ROUND_DIGITS, RoundingMode.HALF_UP).toString());
			logger.info("Result of calculation " + computationData.getIdentifier() + " is " + dose);
			return result;
			
		} catch (Exception e) {
			logger.error("Unable to perform DvhDoseComputation for voiName=" + voiName 
					+ ", containerId=" + computationData.getContainerId(), e);
			result.setError(e.getMessage());
			return result;
		}
	}

	public BigDecimal calculateDvhD(DoseVolumeHistogram dvh,
			DvhDoseConfiguration dvhConfiguration,
			BigDecimal targetPrescriptionDose) 
					throws IllegalArgumentException {
		
		BigDecimal dose = new BigDecimal(0);
		BigDecimal[] vVolume = dvh.getvVolume();
		BigDecimal[] vDose = dvh.getvDose();
		BigDecimal limit = dvhConfiguration.getLimit();

		boolean absoluteOutput = dvhConfiguration.getAbsoluteOutput();
		String limitUnit = dvhConfiguration.getLimitUnit();

		if (absoluteOutput && CC.equals(limitUnit)) {
			dose = dvhService.computeAbsDoseWithAbsVolume(vVolume, vDose, limit);
		} else if (absoluteOutput && PERCENT.equals(limitUnit)) {
			dose = dvhService.computeAbsDoseWithRelVolume(vVolume, vDose, limit);
		} else if (!absoluteOutput && PERCENT.equals(limitUnit)) {
			dose = dvhService.computeRelDoseWithRelVolume(vVolume, vDose, limit, targetPrescriptionDose);
		} else if (!absoluteOutput && CC.equals(limitUnit)) {
			dose = dvhService.computeRelDoseWithAbsVolume(vVolume, vDose, limit, targetPrescriptionDose);
		} else {
			throw new IllegalArgumentException("Unable to calculate DVH-D parameter for dvhConfiguration=" 
					+ dvhConfiguration.toString());
		}
		return dose;
	}

	@Override
	public String getServiceName() {
		return SERVICE_NAME;
	}

	@Override
	public RequirementData getRequirements() {
		RequirementData reqData = new RequirementData();
		reqData.setModalities(new HashSet<>(Arrays.asList(new String[]{"CT", "RTSTRUCT", "RTDOSE"})));
		return reqData;
	}

	private static class DvhDoseConfiguration {
		BigDecimal limit;
		boolean absoluteOutput;
		String limitUnit;

		public BigDecimal getLimit() {
			return limit;
		}
		public void setLimit(BigDecimal limit) {
			this.limit = limit;
		}
		public boolean getAbsoluteOutput() {
			return absoluteOutput;
		}
		public void setAbsoluteOutput(boolean absoluteOutput) {
			this.absoluteOutput = absoluteOutput;
		}
		public String getLimitUnit() {
			return limitUnit;
		}
		public void setLimitUnit(String limitUnit) {
			this.limitUnit = limitUnit;
		}
		@Override
		public String toString() {
			return "DvhDoseConfiguration [limit=" + limit + ", absoluteOutput=" + absoluteOutput + ", limitUnit="
					+ limitUnit + "]";
		}
	}
}
