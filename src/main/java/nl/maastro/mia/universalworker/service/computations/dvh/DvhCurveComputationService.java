package nl.maastro.mia.universalworker.service.computations.dvh;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.service.interfaces.RequirementData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static nl.maastro.mia.universalworker.service.CreateStructureDataService.DVH_DATA;

@Service("DvhCurveComputation")
public class DvhCurveComputationService implements ComputationInterface{
	private static final Logger logger = Logger.getLogger(DvhCurveComputationService.class);

	public static final String SERVICE_NAME = "DvhCurveComputation";
	private static final String IS_ABSOLUTE = "isAbsolute";
	private static final String VOLUME_VECTOR = "volumevector";
	private static final String DOSE_VECTOR = "dosevector";
	private static final String BINSIZE = "binsize";

	@Autowired
	ObjectMapper jsonMapper;
	
	@Autowired
	private GenericComputationToolService genericComputationToolService;

	@Override
	public ComputationResult performComputation(ComputationData computationData) {
		ComputationResult result = new ComputationResult();
		result.setModuleName(SERVICE_NAME);
		result.setVersion(genericComputationToolService.getWorkerVersion());
		result.setExportResult(true);
		
		@SuppressWarnings("unchecked")
		HashMap<String, DoseVolumeHistogram> dvhMap = 
				(HashMap<String, DoseVolumeHistogram>) computationData.getData().get(DVH_DATA);
		
		// At the moment all computations for the MIA have at most one volume of interest
		String voiName = computationData.getVolumesOfInterest().get(0).getName();
		
		try {
			DoseVolumeHistogram dvh = dvhMap.get(voiName);
			if (dvh == null || dvh.getvVolume() == null || dvh.getvDose() == null) {
				throw new Exception("No DVH created for volume of interest: " + voiName);
			}
			
			DvhCurveConfiguration dvhCurveConfiguration = jsonMapper.readValue(computationData.getConfiguration(), DvhCurveConfiguration.class);
			boolean absolute = dvhCurveConfiguration.isAbsolute();
			
			Map<String, String> dvhResults;
			if (absolute) {
				dvhResults = addAbsoluteDvhToResults(dvh);
			} else {
				dvhResults = addNonAbsoluteDvhToResults(dvh);
			}
			
			if(dvhResults.isEmpty()){
				result.setError("No DVH created for voi " + voiName);
			}
			result.getValues().putAll(dvhResults);
			return result;
		} catch (Exception e) {
			logger.error("Unable to perform DvhCurveComputation for voiName=" + voiName 
					+ ", containerId=" + computationData.getContainerId(), e);
			result.setError(e.getMessage());
			return result;
		}
	}
	
	public Map<String,String> addAbsoluteDvhToResults(DoseVolumeHistogram doseVolumeHistogram) {
		if(doseVolumeHistogram.getvVolume() == null || doseVolumeHistogram.getvDose() == null){
			logger.warn("DVH vectors could not be created for at least one VOI.");
			return Collections.emptyMap(); 
		}
		Map<String, String> dvhResults = getDoseVectorAndBinSize(doseVolumeHistogram);

		BigDecimal[] roundedValues = Arrays.stream(doseVolumeHistogram.getvVolume())
				.map(v -> v.setScale(4,RoundingMode.HALF_UP))
				.toArray(BigDecimal[]::new);
		dvhResults.put(VOLUME_VECTOR, Arrays.toString(roundedValues));
		dvhResults.put(IS_ABSOLUTE, Boolean.toString(true));
		return dvhResults;
	}
	
	public Map<String,String> addNonAbsoluteDvhToResults(DoseVolumeHistogram doseVolumeHistogram) {
		if(doseVolumeHistogram.getvVolume() == null || doseVolumeHistogram.getvDose() == null){
			logger.warn("DVH vectors could not be created for at least one VOI.");
			return Collections.emptyMap(); 
		}
		Map<String, String> dvhResults = getDoseVectorAndBinSize(doseVolumeHistogram);
		dvhResults.put(VOLUME_VECTOR, Arrays.toString(doseVolumeHistogram.getvVolumeRelative()));
		dvhResults.put(IS_ABSOLUTE, Boolean.toString(false));
		return dvhResults;
	}
	
	public Map<String,String> getDoseVectorAndBinSize(DoseVolumeHistogram doseVolumeHistogram) {
		Map<String,String> map = new HashMap<>();
		double binSize = doseVolumeHistogram.getBinSize();		
		map.put(BINSIZE, Double.toString(binSize));

		BigDecimal[] roundedValues = Arrays.stream(doseVolumeHistogram.getvDose())
				.map(v -> v.setScale(4,RoundingMode.HALF_UP))
				.toArray(BigDecimal[]::new);
		map.put(DOSE_VECTOR, Arrays.toString(roundedValues));
		return map;
	}
	
	@Override
	public String getServiceName() {
		return SERVICE_NAME;
	}
	
	@Override
	public RequirementData getRequirements() {
		RequirementData reqData = new RequirementData();
		reqData.setModalities(new HashSet<>(Arrays.asList(new String[]{"CT", "RTSTRUCT", "RTDOSE"})));
		return reqData;
	}
	
	private static class DvhCurveConfiguration {
		boolean absolute;
		
		public boolean isAbsolute(){
			return this.absolute;
		}
		public void setAbsolute(boolean absolute){
			this.absolute = absolute;
		}
	}
}
