package nl.maastro.mia.universalworker.service.computations;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.mia.universalworker.config.RuntimeComputationProperties;
import nl.maastro.mia.universalworker.service.FileService;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.universalworker.web.dto.RuntimeComputationConfigurationDto;
import nl.maastro.mia.workerplugin.constants.DicomConcepts;
import nl.maastro.mia.workerplugin.container.ComputationData;
import nl.maastro.mia.workerplugin.container.ComputationResult;
import nl.maastro.mia.workerplugin.container.Image;
import nl.maastro.mia.workerplugin.service.communication.ConfigurationService;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import nl.maastro.mia.workerplugin.service.interfaces.RequirementData;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RuntimeComputationService implements ComputationInterface {

    private static final Logger logger = Logger.getLogger(RuntimeComputationService.class);

    public static final String SERVICE_NAME = "RuntimeComputation";
    
    private static final String PLACEHOLDER_COMPUTATIONDATA = "computationdata";
    private static final String PLACEHOLDER_CT = "ct";
    private static final String PLACEHOLDER_RTSTRUCT = "rtstruct";
    private static final String PLACEHOLDER_RTPLAN = "rtplan";
    private static final String PLACEHOLDER_RTDOSE = "rtdose";
    private static final String PLACEHOLDER_VOI = "voi";
    
    // Placeholders in the runtime command are identified by the regexp pattern ${placeholder}
    private static final String PLACEHOLDER_REGEXP = "\\$\\{(.*?)\\}";

    private ObjectMapper objectMapper;
    private ConfigurationService configurationService;
    private GenericComputationToolService genericComputationToolService;
    private FileService fileService;

    private RuntimeComputationProperties runtimeComputationProperties;

    public RuntimeComputationService(ObjectMapper objectMapper,
                                     GenericComputationToolService genericComputationToolService,
                                     ConfigurationService configurationService,
                                     FileService fileService,
                                     RuntimeComputationProperties runtimeComputationProperties) {
        this.objectMapper = objectMapper;
        this.configurationService = configurationService;
        this.genericComputationToolService = genericComputationToolService;
        this.fileService = fileService;
        this.runtimeComputationProperties = runtimeComputationProperties;
    }

    @Override
    public ComputationResult performComputation(ComputationData computationData) {

        ComputationResult result = new ComputationResult();
        result.setModuleName(SERVICE_NAME);
        
        try {
            RuntimeComputationConfigurationDto configuration = objectMapper.readValue(
                    computationData.getConfiguration(), RuntimeComputationConfigurationDto.class);
            
            String runtimeComputationName = configuration.getRuntimeComputationName();
            if (runtimeComputationProperties == null) {
                throw new Exception("Unknown RuntimeComputation: " + runtimeComputationName);
            }
            
            Set<String> requiredModalities = new HashSet<>(runtimeComputationProperties.getRequiredModalities());
            downloadRequiredModalities(requiredModalities, computationData);
            Map<String, Path> extConfigFiles = configurationService.downloadExtConfigFiles(
                    configuration.getConfigFiles(), Paths.get(computationData.getTempPath()));
            
            List<String> command = resolveCommand(runtimeComputationProperties.getCommand(), computationData, extConfigFiles);
            String workingDirectory = getWorkingDirectory(runtimeComputationProperties, computationData.getTempPath());
            
            result = runCommand(command, workingDirectory, result);
        } catch (Exception e) {
            logger.error("Unable to perform RuntimeModule for containerId=" + computationData.getContainerId(), e);
            result.setError(e.getMessage());
        }
        
        return result;
    }
    
    private void downloadRequiredModalities(Set<String> requiredModalities, ComputationData computationData) throws Exception {
        Map<String, List<Image>> images = computationData.getImages();
        genericComputationToolService.checkRequiredModalitiesAreAvailable(requiredModalities, images);
        for(String requiredModality : requiredModalities){
            fileService.downloadModality(requiredModality, images, Paths.get(computationData.getTempPath()));
        }
    }
    
    private String getWorkingDirectory(RuntimeComputationProperties runtimeComputation, String defaultWorkingDirectory) {
        String workingDirectoryRelative = runtimeComputation.getWorkingDirectory();
        if(workingDirectoryRelative==null || workingDirectoryRelative.isEmpty())
            workingDirectoryRelative = defaultWorkingDirectory;
        return Paths.get(workingDirectoryRelative).toAbsolutePath().normalize().toString();
    }

    public List<String> resolveCommand(List<String> command, 
            ComputationData computationData, 
            Map<String, Path> extConfigFiles) throws Exception {
        List<String> resolvedCommand = new ArrayList<String>(command);
        Pattern regexPattern = Pattern.compile(PLACEHOLDER_REGEXP);
        for (int i = 0; i < resolvedCommand.size(); i++) {
            String commandElement = resolvedCommand.get(i);
            Matcher matcher = regexPattern.matcher(commandElement);
            if (matcher.find()) {
                String placeholder = matcher.group(1);
                try {
                    resolvedCommand.set(i, resolvePlaceholder(placeholder, computationData, extConfigFiles));
                } catch (Exception e) {
                    throw new Exception("Unable to resolve placeholder '" + placeholder + "' in command: " + command);
                }
            }
        }
        return resolvedCommand;
    }
    
    public String resolvePlaceholder(String placeholder, 
            ComputationData computationData, 
            Map<String, Path> extConfigFiles) throws Exception {
        switch (placeholder.toLowerCase()) {
        case PLACEHOLDER_COMPUTATIONDATA:
            return writeComputationDataJson(computationData);
        case PLACEHOLDER_CT:
            return getImagePath(DicomConcepts.CT, computationData.getImages(), computationData.getTempPath());
        case PLACEHOLDER_RTSTRUCT:
            return getImagePath(DicomConcepts.RTSTRUCT, computationData.getImages(), computationData.getTempPath());
        case PLACEHOLDER_RTPLAN:
            return getImagePath(DicomConcepts.RTPLAN, computationData.getImages(), computationData.getTempPath());
        case PLACEHOLDER_RTDOSE:
            return getImagePath(DicomConcepts.RTDOSE, computationData.getImages(), computationData.getTempPath());
        case PLACEHOLDER_VOI:
            String volumesOfInterestJson = objectMapper.writeValueAsString(computationData.getVolumesOfInterest());
            return volumesOfInterestJson;
        default:
            if (extConfigFiles.containsKey(placeholder)) {
                return extConfigFiles.get(placeholder).toString();
            } else {
                throw new Exception("Unable to resolve unknown placeholder: " + placeholder);
            }
        }
    }
    
    public String writeComputationDataJson(ComputationData computationData) throws IOException {
        Path path = Paths.get(computationData.getTempPath()).toAbsolutePath().normalize().resolve("computationData.json");
        objectMapper.writeValue(path.toFile(), computationData);
        return path.toString();
    }
    
    private String getImagePath(String modality, 
            Map<String, List<Image>> images, String tempPath) throws IOException, NoSuchElementException {

        List<Image> imageList = images.get(modality.toString());
        if (imageList == null || imageList.isEmpty()) {
            throw new NoSuchElementException("No images found for modality: " + modality);
        }
        
        switch (modality) {
        case DicomConcepts.CT:
            File firstCtImage = new File(imageList.get(0).getLocation());
            return firstCtImage.getParent();
        default:
            return imageList.get(0).getLocation();
        }
    }
        
    public ComputationResult runCommand(List<String> command, 
            String workingDirectory, 
            ComputationResult result) 
        throws IOException, InterruptedException {
        
        logger.info("Runtime exec --> command:" + command);
        
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(command);
        builder.directory(new File(workingDirectory));
        builder.redirectErrorStream(true);
        Process process = builder.start();
        
        BufferedReader inputStreamReader = null;
        try {
            inputStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = inputStreamReader.readLine()) != null) {
                logger.info(">>> " + line);
            }
        } catch (IOException e) {
            logger.warn("Error while reading input stream of process: " + process, e);
        } finally {
            try {
                inputStreamReader.close();
            } catch (IOException e) { }
        }
        
        int status = process.waitFor();
        if (status != 0) {
            String errorStream = readErrorStream(process.getErrorStream());
            String msg = "RuntimeModule terminated abnormally with exit status: " + status + ", errorStream:" + errorStream;
            logger.error(msg);
            result.setError(msg);
        }
        
        return result;
    }

    private String readErrorStream(InputStream errorStream) {
        String errorStreamString;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(errorStream));
            errorStreamString = reader.lines().collect(Collectors.joining(System.lineSeparator()));
        } finally {
            try {
                reader.close();
            } catch (IOException e) {}
        }
        return errorStreamString;
    }    
    
    @Override
    public RequirementData getRequirements() {
        RequirementData reqData = new RequirementData();
        reqData.setModalities(new HashSet<>(runtimeComputationProperties.getRequiredModalities()));
        return reqData;
    }
    
    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }
}
