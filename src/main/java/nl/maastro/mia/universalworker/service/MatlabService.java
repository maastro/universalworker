package nl.maastro.mia.universalworker.service;

import calculateDose.DvhUtilities;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mathworks.toolbox.javabuilder.*;
import nl.maastro.mia.universalworker.config.ComputationProperties;
import nl.maastro.mia.universalworker.entities.DoseVolumeHistogram;
import nl.maastro.mia.workerplugin.container.VolumeOfInterest;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service
@EnableConfigurationProperties(ComputationProperties.class)
public class MatlabService {
	
	private ComputationProperties computationProperties;
	
	private ObjectMapper jsonMapper;

	public MatlabService(ComputationProperties computationProperties,
						 ObjectMapper jsonMapper){
		this.computationProperties = computationProperties;
		this.jsonMapper = jsonMapper;
	}
	
	public Map<String, DoseVolumeHistogram> createDvhs(Map<String, VolumeOfInterest> volumesOfInterest, 
			DvhUtilities dvhUtilities,
			MWMatrixRef rtStruct,
			MWMatrixRef referenceImage) throws MWException, IOException {
		Map<String, DoseVolumeHistogram> dvhs = new HashMap<>();
		for(Entry<String, VolumeOfInterest> voiEntry : volumesOfInterest.entrySet()){
			VolumeOfInterest volumeOfInterest = voiEntry.getValue();
			MWCellArray roiNames = convertStringListToMWCellArray(volumeOfInterest.getRois());
			MWCellArray operators = convertStringListToMWCellArray(volumeOfInterest.getOperators());
			DoseVolumeHistogram newDvh = createDvh(rtStruct,
					referenceImage,
					roiNames,
					operators,
					dvhUtilities);
			dvhs.put(voiEntry.getKey(), newDvh);
		}
		return dvhs;
	}

	public Map<String, DoseVolumeHistogram> createDvhs(Map<String, VolumeOfInterest> volumesOfInterest, 
			DvhUtilities dvhUtilities,
			MWMatrixRef rtStruct, 
			MWMatrixRef referenceDose, 
			MWMatrixRef referenceImage) throws MWException, IOException {
		Map<String, DoseVolumeHistogram> dvhs = new HashMap<>();
		for(Entry<String, VolumeOfInterest> voiEntry : volumesOfInterest.entrySet()){
			VolumeOfInterest volumeOfInterest = voiEntry.getValue();
			MWCellArray roiNames = convertStringListToMWCellArray(volumeOfInterest.getRois());
			MWCellArray operators = convertStringListToMWCellArray(volumeOfInterest.getOperators());
			DoseVolumeHistogram newDvh = createDvh(rtStruct,
					referenceDose,
					referenceImage,
					roiNames,
					operators,
					dvhUtilities);
			dvhs.put(voiEntry.getKey(), newDvh);
		}
		return dvhs;
	}
	
	public DoseVolumeHistogram createDvh(MWMatrixRef rtStruct,
			MWMatrixRef referenceImage,
			MWCellArray roiNames,
			MWCellArray operators,
			DvhUtilities dvhUtilities) 
					throws MWException, IOException {
		Object[] dvhInput = new Object[] {rtStruct, 
				referenceImage, roiNames, operators};
		MWCharArray json = (MWCharArray) dvhUtilities
				.createDoseVolumeHistogramNoDose(1, dvhInput)[0];
		String jsonString = json.toString();
		return jsonMapper.readValue(jsonString, DoseVolumeHistogram.class);
	}

	public DoseVolumeHistogram createDvh(MWMatrixRef rtStruct,
			MWMatrixRef referenceDose,
			MWMatrixRef referenceImage,
			MWCellArray roiNames,
			MWCellArray operators,
			DvhUtilities dvhUtilities)
					throws MWException, IOException{
	    double binSize = computationProperties.getBinSize();
		Object[] dvhInput = new Object[] {rtStruct, referenceDose, 
				referenceImage, roiNames, operators, binSize};
		MWCharArray json = (MWCharArray) dvhUtilities.createDoseVolumeHistogram(1, dvhInput)[0];
		String jsonString = json.toString();
		return jsonMapper.readValue(jsonString, DoseVolumeHistogram.class);
	}
	
	public MWMatrixRef createRtStruct(String structLocation, DvhUtilities dvhUtilities) 
			throws MWException {
		Object[] rtStructResult = dvhUtilities.createRtStruct(1, structLocation);
		return (MWMatrixRef) rtStructResult[0];
	}
	
	public MWMatrixRef createReferenceImage(MWStructArray ctProperties, DvhUtilities dvhUtilities)
			throws MWException {
		Object[] referenceImageResult = dvhUtilities.createImageFromCtProperties(1, ctProperties);
		return (MWMatrixRef) referenceImageResult[0];
	}
	
	public MWMatrixRef createReferenceDose(String doseLocation, BigDecimal targetPrescriptionDose, MWMatrixRef referenceImage,
										   DvhUtilities dvhUtilities) throws MWException {

		Float targetPrescriptionDoseFloat = targetPrescriptionDose.floatValue();
		Object[] referenceDoseResult = dvhUtilities.createReferenceDose(1, doseLocation, targetPrescriptionDoseFloat, referenceImage);
		return (MWMatrixRef) referenceDoseResult[0];
	}
	
	public static MWCellArray convertStringListToMWCellArray(List<String> list) {
		MWCellArray out = new MWCellArray(1, list.size());
		for (int i=0; i<list.size(); i++) {
			MWCharArray mwChar = new MWCharArray(list.get(i));
			out.set(i+1, mwChar); 	// MWCellArray uses 1-based indexing
		}
		return out;
	}
}
