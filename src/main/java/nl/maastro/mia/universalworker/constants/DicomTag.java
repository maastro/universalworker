package nl.maastro.mia.universalworker.constants;

public class DicomTag {
    
    public static final String SLICE_THICKNESS = "00180050";
    public static final String IMAGE_POSITION_PATIENT = "00200032";
    public static final String IMAGE_ORIENTATION_PATIENT = "00200037";
    public static final String ROWS = "00280010";
    public static final String COLUMNS = "00280011";
    public static final String PIXEL_SPACING = "00280030";
    public static final String DOSE_REFERENCE_SEQUENCE = "300A0010";
    public static final String DOSE_REFERENCE_TYPE = "300A0020";
    public static final String TARGET_PRESCRIPTION_DOSE = "300A0026";
    
}
