package nl.maastro.mia.universalworker;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import nl.maastro.mia.workerplugin.service.Pipeline;

@SpringBootApplication
@EnableEurekaClient
public class UniversalWorkerApplication {

	private static final Logger logger = Logger.getLogger(UniversalWorkerApplication.class);
	public static final String name = UniversalWorkerApplication.class.getPackage().getName();
	public static final String version = UniversalWorkerApplication.class.getPackage().getImplementationVersion();
	
	private static final String nameGeneric = Pipeline.class.getPackage().getName();
	private static final String versionGeneric = Pipeline.class.getPackage().getImplementationVersion();

	public static void main(String[] args) {    	
		SpringApplication.run(UniversalWorkerApplication.class, args);
		logger.info("**** universal worker VERSION **** : " + name + " " +  version );  
		logger.info("**** Pipeline VERSION **** : " + nameGeneric + " " +  versionGeneric );
	}
}