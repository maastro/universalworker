package nl.maastro.mia.universalworker.config;

import java.util.List;

public class RuntimeComputationProperties {

    private List<String> command;
    private String workingDirectory;
    private List<String> requiredModalities;
    
    public List<String> getCommand() {
        return command;
    }
    public void setCommand(List<String> command) {
        this.command = command;
    }
    public String getWorkingDirectory() {
        return workingDirectory;
    }
    public void setWorkingDirectory(String workingDirectory) {
        this.workingDirectory = workingDirectory;
    }
    public List<String> getRequiredModalities() {
        return requiredModalities;
    }
    public void setRequiredModalities(List<String> requiredModalities) {
        this.requiredModalities = requiredModalities;
    }
    @Override
    public String toString() {
        return "RuntimeComputation [command=" + command + ", workingDirectory=" + workingDirectory
                + ", requiredModalities=" + requiredModalities + "]";
    }
}
