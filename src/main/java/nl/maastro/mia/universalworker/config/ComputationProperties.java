package nl.maastro.mia.universalworker.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.util.HashMap;

@ConfigurationProperties(prefix="computations")
public class ComputationProperties {

    @NestedConfigurationProperty
    private HashMap<String, RuntimeComputationProperties> runtimeComputations = new HashMap<>();

    public HashMap<String, RuntimeComputationProperties> getRuntimeComputations() {
        return runtimeComputations;
    }

    private double binSize = 0.01;

    public double getBinSize() {
        return binSize;
    }

    public void setBinsize(double binSize) {
        this.binSize = binSize;
    }

    public void setRuntimeComputations(HashMap<String, RuntimeComputationProperties> runtimeComputations) {
        this.runtimeComputations = runtimeComputations;
    }
}
