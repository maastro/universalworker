package nl.maastro.mia.universalworker.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.mia.universalworker.service.FileService;
import nl.maastro.mia.universalworker.service.GenericComputationToolService;
import nl.maastro.mia.universalworker.service.computations.RuntimeComputationService;
import nl.maastro.mia.workerplugin.service.communication.ConfigurationService;
import nl.maastro.mia.workerplugin.service.interfaces.ComputationInterface;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Configuration
@EnableConfigurationProperties(ComputationProperties.class)
public class RuntimeComputationConfiguration implements ApplicationContextAware {

    private static final Logger logger = Logger.getLogger(RuntimeComputationConfiguration.class);

    private ConfigurableBeanFactory beanFactory;

    private ObjectMapper objectMapper;

    private ConfigurationService configurationService;
    private FileService fileService;
    private GenericComputationToolService genericComputationToolService;

    private final ComputationProperties computationProperties;


    public RuntimeComputationConfiguration(ObjectMapper objectMapper,
                                           ComputationProperties computationProperties,
                                           FileService fileService,
                                           ConfigurationService configurationService,
                                           GenericComputationToolService genericComputationToolService){
        this.objectMapper = objectMapper;
        this.computationProperties = computationProperties;
        this.fileService = fileService;
        this.configurationService = configurationService;
        this.genericComputationToolService = genericComputationToolService;
    }

    @PostConstruct
    private void init(){
        HashMap<String, RuntimeComputationProperties> runtimeComputations = computationProperties.getRuntimeComputations();
        runtimeComputations.keySet().forEach(runtimeComputationName -> {
            createBean(runtimeComputationName, runtimeComputations.get(runtimeComputationName));
        } );

    }

    private void createBean(String moduleName, RuntimeComputationProperties runtimeComputationProperties){
        logger.info("Create Bean for RuntimeComputation: " + moduleName);

        ComputationInterface computation = new RuntimeComputationService(
                objectMapper,
                genericComputationToolService,
                configurationService,
                fileService,
                runtimeComputationProperties);

        beanFactory.registerSingleton(moduleName, computation);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
         this.beanFactory = (ConfigurableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
    }
}
