# MIA Universal Worker #

The universal worker is a microservice within the [Medical Image Analysis (MIA)] (https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home) framework. The core functionality of the universal worker is the computation of dose-volume histograms and associated metrics such as min/mean/max dose, and Vx- and Dx-parameters. The computation modules are wrappers around Matlab code, compiled using the [Matlab Compiler SDK](http://nl.mathworks.com/products/matlab-compiler-sdk/). Computation results are exported to the [MIA Resultservice](https://bitbucket.org/maastrosdt/resultservice).

## Prerequisites ##

- Windows installation
- Java 8
- [Matlab MCR 9](http://nl.mathworks.com/products/compiler/mcr/)
- 4 GB RAM

## Available computations ##

###### DVH computations

Dose-volume histograms and associated metrics can be computed for configured volumes of interests. The following computations are supported.

- VolumeComputation: computes the total volume
- DoseComputation: computes the min/mean/max dose 
- DvhCurveComputation: computes the dose-volume-histogram curve 
- DvhVolumeComputation: computes a Vx-parameter 
- DvhDoseComputation: computes a Dx-parameter

###### Runtime computations

The universal worker also supports so-called runtime computations. This enables for example to call your own batch or python script for performing computations on DICOM data. For information on creating a runtime computation see [setting up a runtime computation](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Setting%20up%20a%20Runtime%20Computation).

## Configuration ##

Configuration can be set via YAML configuration files.

##### Binsize

The size of the dose bins (in Gray) for the dose-volume-histogram computations can be configured by adding the following setting to your application.yml file. The default value is 0.01.

    computations:
        binsize: 0.01

## Development notes ##

Please refer to the [worker plugin documentation](https://bitbucket.org/maastrosdt/worker-plugin) for development notes.